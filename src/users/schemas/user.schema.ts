import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Transaction } from 'src/transactions/schemas/transaction.schemas';
import { SchemaTypes } from 'mongoose'; // Import SchemaTypes

@Schema({ timestamps: true })
export class User extends Document {
  @Prop({ required: true })
  username: string;

  @Prop({ required: true })
  password: string;

  @Prop({ type: [{ type: SchemaTypes.ObjectId, ref: 'Account' }] })
  accounts: string[]; // Array of Account IDs

  @Prop({ type: [{ type: SchemaTypes.ObjectId, ref: 'Transaction' }] })
  transactions: Transaction[]; // Array of Transaction objects
}

export const UserSchema = SchemaFactory.createForClass(User);
