import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Account } from './schemas/account.schemas';
import { CreateAccountDto } from './dto/create-account.dto';
import { UsersService } from '../users/users.service'; // Import UsersService
import { User } from 'src/users/schemas/user.schema';
import { UpdateAccountDto } from './dto/update-account.dto';

@Injectable()
export class AccountsService {
  constructor(
    @InjectModel(Account.name) private accountModel: Model<Account>,
    @InjectModel(User.name) private userModel: Model<User>,
    private usersService: UsersService, // Inject UsersService
  ) {}

  async createAccount(accountData: CreateAccountDto): Promise<Account> {
    const createdAccount = new this.accountModel(accountData);
    const savedAccount = await createdAccount.save();

    // Update user's account array with the new account object
    await this.userModel.findByIdAndUpdate(
      accountData.owner, // Assuming you have owner's ID
      { $push: { accounts: savedAccount } },
      { new: true },
    );

    return savedAccount;
  }

  async getAllAccounts(): Promise<Account[]> {
    return this.accountModel.find().exec();
  }

  async getAccount(id: string): Promise<Account> {
    return this.accountModel.findById(id).exec();
  }

  async findByOwnerId(ownerId: string): Promise<Account[]> {
    const account = await this.accountModel.find({ owner: ownerId }).exec();
    return account;
  }

  async update(id: string, updateAccountDto: UpdateAccountDto) {
    console.log(id);

    if ('balance' in updateAccountDto && updateAccountDto.balance < 0) {
      throw new BadRequestException('Balance cannot be lower than 0');
    }

    const updatedAccount = await this.accountModel
      .findByIdAndUpdate(
        id,
        updateAccountDto,
        { new: true }, // To get the updated document after the update
      )
      .exec();

    if (!updatedAccount) {
      throw new NotFoundException();
    }

    return updatedAccount;
  }
}
