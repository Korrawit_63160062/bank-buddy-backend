import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AccountsService } from './accounts.service';
import { Account } from './schemas/account.schemas';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { UpdateAccountDto } from './dto/update-account.dto';

@UseGuards(JwtAuthGuard)
@Controller('accounts')
export class AccountsController {
  constructor(private readonly accountsService: AccountsService) {}

  @Post()
  create(@Body() accountData: Account) {
    return this.accountsService.createAccount(accountData);
  }

  @Get()
  getAllAccounts() {
    return this.accountsService.getAllAccounts();
  }

  @Get(':id')
  getAccount(@Param('id') id: string) {
    return this.accountsService.getAccount(id);
  }

  @Get('owner/:ownerId')
  findBySenderId(@Param('ownerId') ownerId: string) {
    return this.accountsService.findByOwnerId(ownerId);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAccountDto: UpdateAccountDto) {
    return this.accountsService.update(id, updateAccountDto);
  }
}
