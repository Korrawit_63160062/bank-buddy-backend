import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Account, AccountSchema } from './schemas/account.schemas';
import { AccountsController } from './accounts.controller';
import { AccountsService } from './accounts.service';
import { UsersModule } from 'src/users/users.module'; // Make sure the path is correct

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Account.name, schema: AccountSchema }]),
    UsersModule, // Import the UsersModule
  ],
  controllers: [AccountsController],
  providers: [AccountsService],
  exports: [MongooseModule], // Export MongooseModule if needed
})
export class AccountsModule {}
