import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class Account extends Document {
  @Prop({ required: true })
  balance: number;

  @Prop({ type: 'ObjectId', ref: 'User', required: true })
  owner: string;
}

export const AccountSchema = SchemaFactory.createForClass(Account);
