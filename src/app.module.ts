import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AccountsModule } from './accounts/accounts.module';
import { TransactionsModule } from './transactions/transactions.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    UsersModule,
    AccountsModule,
    TransactionsModule,
    MongooseModule.forRoot(
      'mongodb+srv://korrawit:5RUIy9DV5pTHvaiQ@cluster0.438eda3.mongodb.net/?retryWrites=true&w=majority',
    ),
    AuthModule,
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
