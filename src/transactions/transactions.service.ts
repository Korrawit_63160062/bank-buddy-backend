import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Transaction } from './schemas/transaction.schemas';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { User } from 'src/users/schemas/user.schema';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectModel(Transaction.name) private transactionModel: Model<Transaction>,
    @InjectModel(User.name) private userModel: Model<User>,
  ) {}

  async createTransaction(
    transactionData: CreateTransactionDto,
  ): Promise<Transaction> {
    if (
      'remain' in transactionData &&
      typeof transactionData.remain === 'number' &&
      transactionData.remain < 0
    ) {
      throw new BadRequestException(
        'You cannot withdraw an amount exceeding your balance',
      );
    }

    const createdTransaction = new this.transactionModel(transactionData);
    const savedTransaction = await createdTransaction.save();

    // Update user's transaction array with the new transaction object

    if (transactionData.type == 'Transfer') {
      const res = await this.userModel.findByIdAndUpdate(
        transactionData.receiverId,
        { $push: { transactions: savedTransaction } },
        { new: true },
      );

      transactionData.remain = transactionData.senderRemain;
      transactionData.side = 'Sender';
      const createdSenderTransaction = new this.transactionModel(
        transactionData,
      );
      const savedSenderTransaction = await createdSenderTransaction.save();
      await this.userModel.findByIdAndUpdate(
        transactionData.sender, // Assuming you have sender's ID
        { $push: { transactions: savedSenderTransaction } },
        { new: true },
      );
    } else {
      await this.userModel.findByIdAndUpdate(
        transactionData.sender, // Assuming you have sender's ID
        { $push: { transactions: savedTransaction } },
        { new: true },
      );
    }

    return savedTransaction;
  }

  async findBySenderId(
    senderId: string,
    accountId: string,
  ): Promise<Transaction[]> {
    const transactions = await this.transactionModel
      .find({ receiver: accountId, side: 'Receiver' }) // Query transactions with sender matching the provided senderId
      .exec();

    return transactions;
  }

  async findTransactionsByUser(userId: string): Promise<Transaction[]> {
    const transactions = await this.transactionModel
      .find({
        $or: [
          { sender: userId, side: 'Sender' }, // Sender matches userId and side is Sender
          { receiver: userId, side: 'Receiver' }, // Receiver matches userAccountId and side is Receiver
        ],
      })
      .populate('sender', 'username') // Populate the sender's username
      .populate('receiver', 'username') // Populate the receiver's username
      .exec();

    return transactions;
  }

  async getAllTransactions(): Promise<Transaction[]> {
    return this.transactionModel
      .find()
      .populate('sender', 'username') // Populate the sender's username
      .populate('receiver', 'username') // Populate the receiver's username
      .exec();
  }
}
