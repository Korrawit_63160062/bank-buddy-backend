import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class Transaction extends Document {
  @Prop({ required: true })
  timestamp: Date;

  @Prop({ type: 'ObjectId', ref: 'User', required: true })
  sender: string; // User ID

  @Prop({ type: 'ObjectId', ref: 'User', required: true })
  receiver: string; // User ID

  @Prop()
  remain: number;

  @Prop({ required: true })
  amount: number;

  @Prop({ required: true, enum: ['Deposit', 'Withdraw', 'Transfer'] })
  type: string; // 'Deposit', 'Withdraw', 'Transfer'

  @Prop({ required: false, enum: ['Sender', 'Receiver', 'User'] })
  side: string;

  receiverId: string;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
