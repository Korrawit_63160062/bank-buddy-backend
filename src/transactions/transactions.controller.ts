import {
  Controller,
  Get,
  Post,
  Body,
  UseGuards,
  Param,
  Query,
} from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('transactions')
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService) {}

  @Post()
  createTransaction(@Body() createTransactionDto: CreateTransactionDto) {
    return this.transactionsService.createTransaction(createTransactionDto);
  }

  @Get()
  getAllTransactions() {
    return this.transactionsService.getAllTransactions();
  }

  @UseGuards(JwtAuthGuard)
  @Get('sender/:senderId/:userAccountId')
  findBySenderId(
    @Param('senderId') senderId: string,
    @Param('userAccountId') userAccountId: string,
  ) {
    return this.transactionsService.findBySenderId(senderId, userAccountId);
  }

  @UseGuards(JwtAuthGuard)
  @Get('transactions/user/:userId')
  findTransactionsByUser(@Param('userId') userId: string) {
    return this.transactionsService.findTransactionsByUser(userId);
  }
}
