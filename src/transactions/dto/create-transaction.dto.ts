export class CreateTransactionDto {
  sender: string;
  senderRemain: number;
  receiver: string;
  receiverId: string;
  amount: number;
  remain: number;
  type: 'Deposit' | 'Withdraw' | 'Transfer';
  side: 'Sender' | 'Receiver';
}
